
/* ********* QUIZ *********
1. Spaghetti Code
2. by using curly braces {}
3. Encapsulation
4. student1.enroll()
5. True
6. it refers to the inner value inside the object 
7. True
8. True
9. True
10. True

*/
//Function Coding
let student1 = {
    name: 'John',
    email: 'john@mail.com',
    grades: [89, 84, 78, 88],
    login:  function() {
        console.log(`${this.email} has logged in`);
    },
    logout: function(){
        console.log(`${this.email} has logged out`);
    },
    listGrades: function (){
            this.grades.forEach(grade => {
            console.log(grade);
        });
    },
    computeAve: function (){
        let ave = 0;
        this.grades.forEach(grade =>{
            ave += grade;
        });

        return(ave/this.grades.length)
    },
    willPass: function (){
        if(this.computeAve() >= 85){
            return true
        } else {
            return false
        }
    },
    willPassWithHonors: function (){
        if(this.computeAve() >= 90){
            return true
        }
    }
};

let student2 = {
    name: 'Joe',
    email: 'joe@mail.com',
    grades: [78, 82, 79, 85],
    login: ()=> {
        console.log(`${this.email} has logged in`);
    },
    logout: function(){
        console.log(`${this.email} has logged out`);
    },
    listGrades: function (){
            this.grades.forEach(grade => {
            console.log(grade);
        });
    },
    computeAve: function (){
        let ave = 0;
        this.grades.forEach(grade =>{
            ave += grade;
        });

        return(ave/this.grades.length)
    },
    willPass: function (){
        if(this.computeAve() >= 85){
            return true
        } else {
            return false
        }
    },
    willPassWithHonors: function (){
        if(this.computeAve() >= 90){
            return true
        }
    }
};

let student3 = {
    name: 'Jane',
    email: 'jane@mail.com',
    grades: [87, 89, 91, 93],
    login: ()=> {
        console.log(`${this.email} has logged in`);
    },
    logout: function(){
        console.log(`${this.email} has logged out`);
    },
    listGrades: function (){
            this.grades.forEach(grade => {
            console.log(grade);
        });
    },
    computeAve: function (){
        let ave = 0;
        this.grades.forEach(grade =>{
            ave += grade;
        });

        return(ave/this.grades.length)
    },
    willPass: function (){
        if(this.computeAve() >= 85){
            return true
        } else {
            return false
        }
    },
    willPassWithHonors: function (){
        if(this.computeAve() >= 90){
            return true
        }
    }
};

let student4 = {
    name: 'Jessie',
    email: 'jessie@mail.com',
    grades: [91, 89, 92, 93],
    login: ()=> {
        console.log(`${this.email} has logged in`);
    },
    logout: function(){
        console.log(`${this.email} has logged out`);
    },
    listGrades: function (){
            this.grades.forEach(grade => {
            console.log(grade);
        });
    },
    computeAve: function (){
        let ave = 0;
        this.grades.forEach(grade =>{
            ave += grade;
        });

        return(ave/this.grades.length)
    },
    willPass: function (){
        if(this.computeAve() >= 85){
            return true
        } else {
            return false
        }
    },
    willPassWithHonors: function (){
        if(this.computeAve() >= 90){
            return true
        }
    }
};

// //create student on,
// let studentOneName = 'John';
// let studentOneEmail = 'john@mail.com';
// let studentOneGrades = [89, 84, 78, 88];

// //create student two
// let studentTwoName = 'Joe';
// let studentTwoEmail = 'joe@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];

// //create student three
// let studentThreeName = 'Jane';
// let studentThreeEmail = 'jane@mail.com';
// let studentThreeGrades = [87, 89, 91, 93];

// //create student four
// let studentFourName = 'Jessie';
// let studentFourEmail = 'jessie@mail.com';
// let studentFourGrades = [91, 89, 92, 93];

// //actions that students may perform will be lumped together
// function login(email){
//     console.log(`${email} has logged in`);
// }

// function logout(email){
//     console.log(`${email} has logged out`);
// }

// function listGrades(grades){
//     grades.forEach(grade => {
//         console.log(grade);
//     })
// }

//This way of organizing employees is not well organized at all.
//This will become unmanageable when we add more employees or functions
//To remedy this, we will create objects